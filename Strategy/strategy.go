package strategy

// В Go можно реализовать паттерн Стратегия, используя интерфейсы и функции как аргументы.

type Strategy func(int, int) int

func Add(a, b int) int {
	return a + b
}

func Substract(a, b int) int {
	return a - b
}

// Это позволяет использовать функции в качестве стратегий.
