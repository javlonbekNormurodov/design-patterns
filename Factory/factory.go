package factory

// Go не имеет классической поддержки для фабричных методов или абстрактных классов,
// но вы можете создать функции, которые создают экземпляры структур или интерфейсов в зависимости от условий.

type Product interface {
	GetName() string
}

type ConcreteProductA struct{}

func (p *ConcreteProductA) GetName() string {
	return "Product A"
}

type ConcreteProductB struct{}

func (p *ConcreteProductB) GetName() string {
	return "Product B"
}

func CreateProduct(productType string) Product {
	if productType == "A" {
		return &ConcreteProductA{}
	} else if productType == "B" {
		return &ConcreteProductB{}
	}
	return nil
}
