package singleton

// Вы можете реализовать паттерн Singleton в Go,
// чтобы обеспечить создание только одного экземпляра объекта.
// Для этого можно использовать пакет-синглтон, см. пример:

type singleton struct {
	data string
}

var instance *singleton

func GetInstance() *singleton {
	if instance == nil {
		instance = &singleton{data: "This is a singleton"}
	}

	return instance
}
