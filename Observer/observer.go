package observer

// В Go вы можете использовать каналы (channels) для реализации паттерна Наблюдатель.
// Один объект (наблюдаемый) может отправлять сообщения другим объектам (наблюдателям) через каналы.

type Observer struct {
	DataChannel chan string
}

func NewObserver() *Observer {
	return &Observer{DataChannel: make(chan string)}
}

func (o *Observer) Update(data string) {
	o.DataChannel <- data
}

type Subject struct {
	Observers []*Observer
}

func NewSubject() *Subject {
	return &Subject{Observers: make([]*Observer, 0)}
}

func (s *Subject) Attach(observer *Observer) {
	s.Observers = append(s.Observers, observer)
}

func (s *Subject) Notify(data string) {
	for _, observer := range s.Observers {
		go observer.Update(data)
	}
}