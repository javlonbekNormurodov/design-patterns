package decorator

// Вы можете использовать композицию структур в Go для реализации паттерна Декоратор, 
// который позволяет добавлять новое поведение объектам без изменения их кода.

type Component interface {
	Operation() string
}

type ConcreteComponent struct {}

func (c *ConcreteComponent) Operation() string {
	return "Concrete Component"
}

type Decorator struct {
	component Component
}

func (d *Decorator) Operation() string {
	return "decorator " + d.component.Operation() 
}